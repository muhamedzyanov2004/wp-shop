<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Custom_Shop
 */

?>

<?php global $redux_demo; ?>

	<footer class="footer">
		<nav>
			<ul>
				<li>
					<span href="#"  class="listTitle">Information</span>
				</li>
				<li>
					<a href="#">The brand</a>
				</li>
				<li>
					<a href="#">Local stores</a>
				</li>
				<li>
					<a href="#">Customer service</a>
				</li>
				<li>
					<a href="#">Privacy & cookies</a>
				</li>
				<li>
					<a href="#">Site map</a>
				</li>
			</ul>
			<ul>
				<li>
					<span href="#"  class="listTitle">Lookbook</span>
				</li>
				<li>
					<a href="#">Latest posts</a>
				</li>
				<li>
					<a href="#">Men’s lookbook</a>
				</li>
				<li>
					<a href="#">Women’s lookbook</a>
				</li>
				<li>
					<a href="#">Lookbooks RSS feed</a>
				</li>
				<li>
					<a href="#">View your lookbook</a>
				</li>
				<li>
					<a href="#">Delete your lookbook</a>
				</li>
			</ul>
			<ul>
				<li>
					<span href="#"  class="listTitle">Your account</span>
				</li>
				<li>
					<a href="#">Sign in</a>
				</li>
				<li>
					<a href="#">Register</a>
				</li>
				<li>
					<a href="#">View cart</a>
				</li>
				<li>
					<a href="#">View your lookbook</a>
				</li>
				<li>
					<a href="#">Track an order</a>
				</li>
				<li>
					<a href="#">Update information</a>
				</li>
			</ul>
			<ul>
				<li>
					<span href="#"  class="listTitle">Why buy from us</span>
				</li>
				<li>
					<a href="#">Shipping & returns</a>
				</li>
				<li>
					<a href="#">Secure shopping</a>
				</li>
				<li>
					<a href="#">Testimonials</a>
				</li>
				<li>
					<a href="#">Award winning</a>
				</li>
				<li>
					<a href="#">Ethical trading</a>
				</li>
			</ul>
			<ul>
				<li>
					<span href="#"  class="listTitle">contact details</span>
				</li>
				<li>
					<a href="#">Head Office: <?php echo $redux_demo['head-office']; ?></a>
				</li>
				<li>
					<a href="#">Telephone: <?php echo $redux_demo['phone-value']; ?></a>
				</li>
				<li>
					<a href="#">Email: <?php echo $redux_demo['email-value']; ?></a>
				</li>
			</ul>
		</nav>
		<div class="footer__box">
			<div class="left">
				<div class="footer__baner">
					<h2 class="footer__title">
						award winner <div>fashion awards 2020</div>
					</h2>
				</div>
			</div>
			<div class="right">
				<div class="footer__social">
					<span>
						<a href="#">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/fcA.png" alt="">
						</a>
					</span>
					<span>
						<a href="#">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/twA.png" alt="">
						</a>
					</span>
					<span>
						<a href="#">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/inA.png" alt="">
						</a>
					</span>
					<span>
						<a href="#">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/piA.png" alt="">
						</a>
					</span>
				</div>
			</div>
		</div>
		<div class="footer__bottom">
			<div class="footer__box">
				<div class="left">
					<span>© 2020 <?php bloginfo( 'name' ); ?>™</span>
				</div>
				<div class="right">
					<span><a href="#">Design by Maksim and Pavel</a></span>
				</div>
			</div>
		</div>
	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

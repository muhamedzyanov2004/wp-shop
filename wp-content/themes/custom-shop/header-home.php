<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Custom_Shop
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>


<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'custom-shop' ); ?></a>

	<header class="header main">
			<div class="header__topBox">
				<div class="header__topBar">
					<div class="header__currency">Currency.GBP</div>
					<div class="header__navBlock">
						<a href="/register/" class="header__navButton">Register</a>
						<a href="/register/" class="header__navButton">Sign In</a>
					<div class="header__empty"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/buy_hov.png">empty <span>&#8744;</span></div>
					</div>
				</div>
			</div>
			<div class="header__mainNavbar">
				<a  href="/" class="header__logo"><?php bloginfo( 'description' ); ?></a>
				<nav class="">
					<ul>
						<?php
							wp_nav_menu(
								array(
									'menu_id'        => 'primary-menu',
								)
							);
							?>
						<form class="header__search">
			  				<input type="text" placeholder="Search..">
			  				<button>
			  					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/loup.png">
			  				</button>
						</form>
					</ul>
				</nav>
			</div>
			<h1 class="header__title">
				<?php bloginfo( 'name' ); ?>
			</h1>
			<button class="colectionBt">
				shop men’s collection
			</button>
		</header>

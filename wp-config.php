<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'WP-Shop' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'wpadmin' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', '123456789' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         't~#Q`l.D3$xfTe}33DuaB9LVCs1CgL_f|7/93JO`&[@MAnonR.I9roDqmHua||&A' );
define( 'SECURE_AUTH_KEY',  '+>|eXR/2aPo3HYAs~)Cvh@LG3$OhU9gSDv0Pm{ZCaF@,djcuZ=OpYb{rg9VjnMpH' );
define( 'LOGGED_IN_KEY',    '-iuw(/d@ZNee>cB9AU/>=zForn5>9[qhZXQe;DuSt2Cl!Qhic6[;&(:rk|(fV]6*' );
define( 'NONCE_KEY',        'M8C.SZ;m35LM#tuWW4e4&:$S[kl`AoKtgVW`zA9^F-Id aEy$)1XR $G6I/C;/g2' );
define( 'AUTH_SALT',        ' um9jYJ8#0,r F#4>.!fEnK,%!nCP&IF-,o*r[Yt/^L<w[fthP42b/K:v+k+Un?1' );
define( 'SECURE_AUTH_SALT', '.Ut+{YO$~,Jf4(kolp1,KDOW>^.Mf<T<fA1~9fvVY:4Om,%_^wD_ 4{QKgtR8l2 ' );
define( 'LOGGED_IN_SALT',   'i*|_V%eX<Qu/gEbs|)lpK&o0Wpr_%Ihi$_,a+|6s9Z?(cLyUb)k&L5g<V:QIpz},' );
define( 'NONCE_SALT',       '$27CkEF_uZ@S,l?=i|qIyOzD+6y JcIEw*3)fQvQ8s/f&8XNPm-bwXBfEp`MEw>N' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
